# SnowDQ

An alternative tactical approach to performing data quality verifications in Snowflake data warehouse


## Documentation
- [Case for SnowDQ](doc/Case_for_SnowDQ.md)
- [Design principles](doc/Design_Principles.md)
- [Installation](doc/installation.md)
- [Developing expectations](doc/developing_expectations.md)
- [Executing expectation suite](doc/Executing_Expectation_Suite.md)

