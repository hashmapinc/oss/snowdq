
# What is an expectation suite ?
Invoking individual expectations on each table one by one would not scale very well. The process would also become tedious and can result in error prone.

We provide a functionality where by you can group a set of expectations together into an "expectation suite". Once configured, you can collectively perform various expectations together.

## Configuring Expectation Suite
Expectation suite are configured in the table [DQ_EXPECTATION_CONFIG](../snowdq/DQ_EXPECTATION_CONFIG.sql). The configuration involves :

- SUITE_ID : Identifies a group of expectations that will verified during an execution.
- EXPECTATION : A name that identifies a specific instance of an expectation.
- SUBJECT_CATALOG : The database where the table exist
- SUBJECT_SCHEMA : The schema where the table exists
- SUBJECT_TABLE : The subject table which onto which, verification is being evaluated.
- POS NUMBER : Determines the order when multiple expectations are run, during a suite execution.
- EXPECTATION_FN : The expectation function that performs the actual verification.
- FN_PARAM : The parameters for the function
- TAGS : Tags, future needs
- DESCRIPTION : Description of what this verification is trying to achieve. Used for documentation needs.
- ENABLED : Indicates if the verification will be executed.
- LAST_UPDATED_TIME : Used for audit or quick checks on when this configuration was declared/updated.

## Invocation
Invoking the expectations involves, calling the stored procedure [VERIFY_EXPECTATIONS](../snowdq/VERIFY_EXPECTATIONS.sql).

### Parameters
- PARAM_SUITE_ID : The verification suite to run.
- PARAM_STORE_RESULT : Should the result be stored in the DQ_VERIFICATION_RUNS table.
- PARAM_METADATA : JSON formatted string, which offers metadata to be added to result, for traceabilibity needs. If provided, should be a valid JSON. The value of this does not affect any functionality.

## Storing verification result
The result of verification can optionally be stored in the table [DQ_VERIFICATION_RUNS](snowdq/DQ_VERIFICATION_RUNS.sql). The result will be stored as json formatted VARIANT.

```json
{
  "EXEC_TIME": "2021-04-05 12:39:31.571000000",
  "EXPECTATION": "STORE_STATE_ALLOWED_VALUES",
  "EXPECTATION_FN": "VERIFY_VALUES_TO_BE_IN_SET",
  "METADATA": {
    "BATCH": 456
  },
  "RESULT": {
    "Failure_error": [],
    "VALIDATION_RESULT": {
      "Failure_error": [],
      "Failures": 0,
      "Success": 1,
      "VALIDATION_FAILED": true,
      "VALIDATION_RESULT": {
        "NOTALLOWED_VALS": "NE,KY,KS,MT,CA,VA,WV,AZ,MO,IN,MS,MA,OR,SC,IA,NC,WI,TX,CO,MN,ND,NM,NH,UT,MI,OH,LA,WA,FL,RI,AK,ME,TN,NJ,PA,MD,NY,ID,VT,SD,IL,DC,CT,OK,AR",
        "NOTALLOWED_VAL_COUNT": 45,
        "VALIDATION_FAILED": true
      }
    }
  },
  "RUN_ID": "210405123931",
  "SUITE_ID": "SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL"
}
```

Each verification is stored as a seperate record. 

### Result keys
 - EXEC_TIME :  Execution timestamp
 - EXPECTATION : The specific verification/expectation for which this result is indicative.
 - EXPECTATION_FN : The underlying expectation function
 - METADATA : An optional parameter, which is passed to offer some context for traceability. For example batch id. The value of this does not affect any functionality.
 - RESULT : The result of the verification.
 - RUN_ID :  An unique id representing the execution run.
 - SUITE_ID : The suite under verification.
 
## View : VERIFICATION_RUN_STATUS_VW
The view [VERIFICATION_RUN_STATUS_VW](../snowdq/VERIFICATION_RUN_STATUS_VW.sql), is a helper function to quickly return the result in a table format.

![](./media/VERIFICATION_RUN_STATUS_VW.png)

## Example 
A sample suite is present in [SUITE_SNOWFLAKE_SAMPLE_DATA-TPCDS_SF100TCL](
../example/SUITE_SNOWFLAKE_SAMPLE_DATA-TPCDS_SF100TCL.sql).

In the below call, we are verifying all expectations configured as part of the suite 'SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL'. We also configured to return the result but not store the same in the DQ_VERIFICATION_RUNS table,

```sql
call VERIFY_EXPECTATIONS('SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL', false, NULL );
```

In the below call, we are verifying all expectations configured as part of the suite 'SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL'. We pass in an optional metadata to help provide some context for later lineage/tracebility purpose. the result gets stored in DQ_VERIFICATION_RUNS table,

```sql
call VERIFY_EXPECTATIONS('SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL', true, parse_json('{ "BATCH":456 }'));
```

![](media/DQ_VERIFICATION_RUNS.jpg)


