## Installation

Installation process involves deploying the various scripts. 

### Base scripts
The scripts found in [snowdq](../snowdq), needs to be executed in the database/schema of choice.

There is no specific order of deployment, hence  open each script file copy and paste into Snowflake worksheet and invoke.

### Verification scripts
Some basic verification/expectation scripts are provided in the folder [../verifications]. There is no specific order required for deployment.

Deploy the script in the same database & schema as where the base tables & procedure were installed.
