

CREATE OR REPLACE TABLE DQ_EXPECTATION_CONFIG (
    SUITE_ID VARCHAR NOT NULL, -- Identifies a group of expectations that will verified during an execution.
    EXPECTATION VARCHAR NOT NULL,	-- A name that identifies a specific instance of an expectation.
    SUBJECT_CATALOG VARCHAR NOT NULL, -- The database where the table exist
    SUBJECT_SCHEMA VARCHAR NOT NULL,  -- The schema where the table exists
    SUBJECT_TABLE VARCHAR NOT NULL,	-- The subject table which onto which, verification is being evaluated.
    POS NUMBER NOT NULL DEFAULT 1,	-- Determines the order when multiple expectations are run, during a suite execution.
    EXPECTATION_FN VARCHAR NOT NULL,	-- The expectation function that performs the actual verification.
    FN_PARAM VARIANT NOT NULL,		-- The parameters for the function
    TAGS VARCHAR,			-- Tags, future needs
    DESCRIPTION VARCHAR,		-- Description of what this verification is trying to achieve. Used for documentation needs.
    ENABLED BOOLEAN NOT NULL DEFAULT TRUE,	-- Indicates if the verification will be executed.
    LAST_UPDATED_TIME TIMESTAMP_LTZ DEFAULT CURRENT_TIMESTAMP() -- used for audit or quick checks on when this configuration was declared/updated.
)
COMMENT='Holds configuration for expectation suite.'
;


