
CREATE OR REPLACE TABLE DQ_VERIFICATION_RUNS(
    VERIFICATIONS VARIANT -- Holds the result of individual verification in a suite execution.
)
COMMENT='Stores the result of verification of a suite execution'
;